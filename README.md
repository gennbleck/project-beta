# CarCar

Team:

*  **Dane Watt** - Auto Sales
*  **Kevin Cedillo** Automobile Service

## Design
CarCar is a web application designed to manage inventory, sales, and services for a car dealership. CarCar is composed of different microservices for inventory, sales, and services these microservices are integrated through polling with the value object being the automobiles. These microservices use RESTful API's to communicate and manage data within eachother. This application uses Docker to organize our services into docker containers, they are separated by REACT front end, database, service API, sales API, inventory API, sales poller, and service poller.

![](/ghi/app/public/555.png)

## How To Run CarCar
1. Fork the project at https://gitlab.com/gennbleck/project-beta
2. Clone the project using http
3. Run the following command in your terminal and make sure your in the directory where you want the project to be
> `git clone https://gitlab.com/gennbleck/project-beta.git`
4. Change your present working directory into the project directory
>`cd project-beta`
5. Open Docker desktop and run the following in your terminal to start
> `docker volume create beta-data`
>
> `docker-compose build`
>
> `docker-compose up`
6. Your all set, now navigate to http://localhost:3000/

## Inventory Microservice ##

The inventory microservice is used for creating and storing vehicle manufacturers, models, and individual automobiles. The Inventory is the base of the CarCar application as the two other microservices, Sales and Service, pull data from the inventory. The Inventory microservice contains 3 models: `Manufacturer`, `VehicleModel`, and `Automobile`. `VehicleModel` contains a foreign key to `Manufacturer` and `Automobile` contains a foreign key to `VehicleModel`. The end goal is to create the `Autombile` model which interects with the other 2 services with the value object, `AutomobileVO` in each service via a poller.

# URL Endpoints for Inventory Microservice

`Manufacturers`
| List Manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a Manufacturer | POST | http://localhost:8100/api/manufacturers/
| Manufacturer Detail | GET | http://localhost:8100/api/manufacturers/:id/
| Update Manufactuer | PUT | http://localhost:8100/api/manufacturers/:id/
| Delete Manufactuer | DELETE | http://localhost:8100/api/manufacturers/:id/

Create Manufacturer { Input JSON }

{
  "name": "Subaru"
}

Create Manufacturer Return

{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Chrysler"
}

List All Manufacturers

{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Subaru"
    }
  ]
}

`Vehicle Models`
| List Models |	GET	| http://localhost:8100/api/models/
| Create Model | POST	| http://localhost:8100/api/models/
| Model Detail | GET | http://localhost:8100/api/models/:id/
| Update Model | PUT	| http://localhost:8100/api/models/:id/
| Delete Model | DELETE | http://localhost:8100/api/models/:id/

Create Model { Input JSON }

{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}

Return Create Model

{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}

Update Model:

{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}

Return Update or Detail Model

{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}

List Models

{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}

`Automobiles`

| List Automobiles | GET | http://localhost:8100/api/automobiles/
| Create Automobile | POST |http://localhost:8100/api/automobiles/
| Get a Specific Automobile | GET | http://localhost:8100/api/automobiles/:vin/
| Update Automobile | PUT | http://localhost:8100/api/automobiles/:vin/
| Delete Automobile | DELETE |http://localhost:8100/api/automobiles/:vin/

Create Automobile { Input JSON }

{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}

Update Automobile { Input JSON }

{
  "color": "red",
  "year": 2012,
  "sold": true
}

Return Update or Detail

{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  },
  "sold": false
}

List All Automobiles

{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      },
      "sold": false
    }
  ]
}

## Service Microservice (localhost:8080)

The Service Microservice is a unit of functionality within the Car dealership's system, consisting of two applications: api and poll, which share a common database. Its main purpose is to manage service appointments and technicians. Additionally, it interacts with the Inventory Microservice to retrieve information about automobiles through a value object called `AutomobileVO`, which is created by polling the Inventory Microservice and uniquely identified by the vehicle identification number (VIN). This allows the Service Microservice to provide VIP service by matching the VIN of the car receiving service. Within the Service Microservice, car dealership staff can maintain records of service appointments, technicians, and customers. The microservice includes three models, each with an automatically assigned database id ("id") and specific attributes.

### Technician

#### Model:
 **Technician**
- first_name,
- last_name,
- employee_id

### HTTP Requests:
> **List Technicians(GET): http://localhost:8080/api/technicians/**

Example JSON Output:

<p>
<code>{
	"technicians": [ <br>
		{    <br>
			"first_name": "Kevin", <br>
			"last_name": "Cedillo", <br>
			"employee_id": "kc2002",<br>
			"id": 2 <br>
		}<br>
	]<br>
     }<br>
    </code>
<code>

    {
	    "technicians": [
		    {
			    "first_name": "Kevin",
			    "last_name": "Cedillo",
			    "employee_id": "kc2002",
			    "id": 2
		    }
	    ]
         }

</code>
</p>

> **Create Technicians(POST): http://localhost:8080/api/technicians/**

Example JSON Input:
<p>
<code>

    {
        "first_name": "Kevin",
	    "last_name": "Cedillo",
	    "employee_id": "SC20"
    }

</code>
</p>
Example JSON Output:
<p>
<code>

    {
	    "technician": {
		    "first_name": "Kevin",
		    "last_name": "Cedillo",
		    "employee_id": "SC20",
		    "id": 3
	    }
    }

</code>
</p>

> **Delete Technician(DELETE): http://localhost:8080/api/technicians/:id**

*In order to delete a technician you must replace the ":id" part with the specific technician id number*

Example JSON Output:
<p>
<code>

    {
	    "delete": true
    }

</code>
</p>

### Appointment

### Model:
 **Appointment**
- status
- vin
- technician
- date_time
- reason
- customer
- is_vip

### HTTP Requests:
> **List Appontments(GET): http://localhost:8080/api/appointments/**

Example JSON output:
<p>
<code>

    {
	    "appointments": [
		    {
			    "href": "/api/appointments/1",
			    "date_time": "2023-06-17T14:03:00+00:00",
			    "reason": "Oil Change",
			    "status": "Canceled",
			    "customer": "Johnson",
			    "technician": {
				    "first_name": "Kevin",
				    "last_name": "Cedillo",
				    "employee_id": "kc2002",
				    "id": 2
			    },
			    "vin": "11221",
			    "is_vip": false,
			    "id": 1
		    }
        ]
    }

</code>
</p>

> **Create Appointments(POST): http://localhost:8080/api/appointments/**

Example JSON Input:

<p>
<code>

    {
	"vin": "1221121212",
	"customer": "Kevin Cedillo",
	"date_time": "2023-06-17T14:03:00+00:00",
	"technician": 2,
	"reason": "Oil change"
    }

</code>
</p>

Example JSON Output:

<p>
<code>

    {
	"appointment": {
		"href": "/api/appointments/20",
		"vin": "11222211121221",
		"status": "Created",
		"technician": {
			"first_name": "Kevin",
			"last_name": "Cedillo",
			"employee_id": "kc2002",
			"id": 2
		},
		"date_time": "2023-04-30T10:45",
		"reason": "Oil change",
		"customer": "Kevin Cedillo",
		"is_vip": false,
		"id": 20
	}
    }

</code>
</p>

> **Update Appointment Status(PUT) (Cancled):http://localhost:8080/api/appointments/:id/cancel**

> **Update Appointment Status(PUT) (Finished):http://localhost:8080/api/appointments/:id/finish**



## Sales microservice ##

Welcome to the Sales microservice of CarCar. The sales microservice has 4 modesl: `AutomobileVO`, `Sale`, `Customer`, and `Salesperson`. The `Sale` pulls info from the other 3 models via foreign keys.

The information required by `AutomobileVO` comes from the Inventory microservice. We set up a poller in Sales microserice that retrieves data from the Inventory microservice (namely vin and sold status from the `Automobile` model) and updates the `AutomobileVO` accordingly in order to be used in the `Sale` model.

# URL Endpoints for Sales Microservice:

`Salespeople`
| List Salespeople | GET | http://localhost:8090/api/salespeople/
| Create Salesperson | POST | http://localhost:8090/api/salespeople/
| Salesperson Detail | GET | http://localhost:8090/api/salespeople/id/
| Delete Salesperson | DELETE | http://localhost:8090/api/salespeople/id/

Create Salesperson{ Input JSON }

{
    "employee_id: "lemonsalesman",
    "first_name": "Dane",
    "last_name": "Watt"
}

Create Salesperson Return

{
	"href": "/api/salespeople/15/",
	"id": 15,
	"employee_id": "lemonsalesman",
	"first_name": "Dane",
	"last_name": "Watt"
}

List all Salespeople Return

{
    "salespeople": [
        {
           "href": "/api/salespeople/15/",
	        "id": 15,
	        "employee_id": "lemonsalesman",
	        "first_name": "Dane",
	        "last_name": "Watt"
        }
    ]
}

`Customers`
| List Customers | GET | http://localhost:8090/api/customers/
| Create Customer | POST | http://localhost:8090/api/customers/
| Customer Detail | GET | http://localhost:8090/api/customers/id/
| Delete Customer | DELETE | http://localhost:8090/api/customers/id/

Create Customer { Input JSON }

{
	"first_name": "Buzz",
	"last_name": "Aldrin",
	"phone_number": "333-222-333",
	"address": "3245 Moon Street"
}

Create Customer Return

{
	"href": "/api/customers/4/",
	"id": 4,
	"first_name": "Buzz",
	"last_name": "Aldrin",
	"phone_number": "333-222-333",
	"address": "3245 Moon Street"
}

List all Customers

{
    "customers": [
        {
            "href": "/api/customers/4/",
	        "id": 4,
	        "first_name": "Buzz",
            "last_name": "Aldrin",
            "phone_number": "333-222-333",
            "address": "3245 Moon Street"
        }
    ]


}

`Sales`

I ran into issues with my Sales microservice. I was unable to get the Create Sale via POST to work, in order to view sales you will have to create sales through the admin.
Go into the project-beta-sales-api-1 Docker container. Then go into the terminal and type 'python manage.py createsuperuser'.
Once the superuser is created go to http://http://localhost:8090/admin/. From there you can submit a sale to be viewed in the get functions as well as the Sales List and Sales History on the front end.


| List Sales | GET | http://localhost:8090/api/sales/
| Create Sale | POST | http://localhost:8090/api/sales/
| Sale Detail | GET | http://localhost:8090/api/sales/id/
| Delete Sale | DELETE | http://localhost:8090/api/sales/id/

Create Sale { Input JSON }

{
	"vin": "23445",
	"salesperson: "Dane Watt",
	"customer": "Buzz Aldrin",
	"price": 555555
}

Return Create Sale
{
	"href": "/api/sales/1/",
	"automobile": {
		"vin": "23445",
		"sold": false
	},
	"salesperson": {
		"href": "/api/salespeople/1/",
		"id": 1,
		"employee_id": "gennbleck",
		"first_name": "Dane",
		"last_name": "Watt"
	},
	"customer": {
		"href": "/api/customers/1/",
		"id": 1,
		"first_name": "Buzz",
		"last_name": "Aldrin",
		"phone_number": "333-222-333",
		"address": "3245 Moon Street"
	},
	"price": 555555
}
List All Sales

{
	"sales": [
		{
			"href": "/api/sales/1/",
			"automobile": {
				"vin": "23445",
				"sold": false
			},
			"salesperson": {
				"href": "/api/salespeople/1/",
				"id": 1,
				"employee_id": "gennbleck",
				"first_name": "Dane",
				"last_name": "Watt"
			},
			"customer": {
				"href": "/api/customers/1/",
				"id": 1,
				"first_name": "Buzz",
				"last_name": "Aldrin",
				"phone_number": "333-222-333",
				"address": "3245 Moon Street"
			},
			"price": 555555
		}
	]
}
