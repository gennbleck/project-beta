
from common.json import ModelEncoder

from .models import Salesperson, Customer, Sale, AutomobileVO

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
    ]
class SalespersonNameEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name"]

    def encode(self, instance):
        salesperson = instance.first_name + " " + instance.last_name
        return {"salesperson": salesperson}

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "employee_id",
        "first_name",
        "last_name"
    ]
    encoders={"salesperson_name": SalespersonNameEncoder(),}

class CustomerNameEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name"]

    def encode(self, instance):
        customer = instance.first_name + " " + instance.last_name
        return {"customer": customer}

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "phone_number",
        "address"
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "automobile",
        "salesperson",
        "customer",
        "price"
    ]
    encoders = {
        # "salesperson": SalespersonEncoder(),
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder()


    }
