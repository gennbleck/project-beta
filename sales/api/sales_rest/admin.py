from django.contrib import admin

# Register your models here.
from .models import Sale, Salesperson, Customer, AutomobileVO
admin.site.register(Sale)
admin.site.register(Salesperson)
admin.site.register(Customer)
# admin.site.register(AutomobileVO)
