import React, { useEffect, useState } from "react";

function SaleHistory() {
    const [ sales, setSales ] = useState ([]);
    const [ employee, setEmployee ] = useState ('');
    const [ employees, setEmployees ] = useState ([]);


    const fetchData = async () => {
        const response = await fetch ('http://localhost:8090/api/sales/');

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
    };

    const handleEmployeeChange = (event) => {
        const value = event.target.value;
        setEmployee(value);
    };

    const fetchSalespeople = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            setEmployees(data.salespeople)
        }
    }

    useEffect(() =>{
        fetchData();
        fetchSalespeople();
    }, []);

    const filteredSales = employee === ''
        ? sales
        : sales.filter(sale => sale.salesperson.employee_id === employee);

    return (
        <main className="container">
            <h1 className="h1-padded">Sales History</h1>
            <div className="mb-3">
                <select onChange={handleEmployeeChange} value={employee} name="employee" id="employee" className="form-select">
                <option value="">All Employees</option>
                    {employees.map(salesperson => (
                    <option value={salesperson.employee_id} key={salesperson.employee_id}>
                        {salesperson.employee_id}
                    </option>
                ))}
                </select>
            </div>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredSales.map((sale, index) => {
                        return (
                            <tr key={index}>
                                <td>{sale.salesperson.employee_id}</td>
                                <td>{sale.salesperson.first_name + " " + sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name + " " + sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </main>
    )
}

export default SaleHistory
