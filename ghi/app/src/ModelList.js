import React, {useEffect, useState} from "react";

function AutoModelList() {
    const [modelList, setModelList] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/models/";

        const response = await fetch (url);

        if (response.ok) {
            const data = await response.json();
            setModelList(data.models);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <main className="container">
            <h1 className="h1-padded">Models</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {modelList.map(model => {
                        return (
                            <tr key={model.href}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td>
                                <img
                      src={model.picture_url}
                      alt=""
                      width="120px"
                      height="120px"
                    />
                  </td>


                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </main>
    )
}

export default AutoModelList
