import React from 'react';
const bg = new URL("/public/bg.png",import.meta.url)

function MainPage() {
  return (
 
    <div className="main">
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Auto Tech</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            The premiere solution for automobile dealership management!
          </p>
        </div>
        {/* <div className='image'>
          <img src={bg} className="pic" />
        </div> */}
      </div>
    </div>

  );
}

export default MainPage;
