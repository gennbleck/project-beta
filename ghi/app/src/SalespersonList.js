import React, { useEffect, useState } from "react";

function SalespersonList() {
    const [salespeople, setSalespeople] = useState([]);

    const fetchData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <main className="container">
      <h1 className="h1-padded">Salespeople</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            {/* <th>Action</th> */}
          </tr>
        </thead>
        <tbody>
          {salespeople.map((salesperson) => (
            <tr key={salesperson.id}>
              <td>{salesperson.employee_id}</td>
              <td>{salesperson.first_name}</td>
              <td>{salesperson.last_name}</td>
              {/* <td>
                <button
                  className="btn btn-danger"
                  onClick={() => handleDelete(salesperson.id)}
                >
                  Delete
                </button>
              </td> */}
            </tr>
          ))}
        </tbody>
      </table>
    </main>
    );


}
export default SalespersonList;
