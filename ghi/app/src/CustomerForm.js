import React, { useState, useEffect } from 'react';

function CustomerForm () {
    const [ firstName, setFirstName ] = useState('');
    const [ lastName, setLastName ] = useState('');
    const [ phoneNumber, setPhoneNumber ] = useState('');
    const [ address, setAddress ] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.first_name = firstName;
        data.last_name = lastName;
        data.phone_number = phoneNumber;
        data.address = address;

        const customerUrl = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();

            setFirstName('');
            setLastName('');
            setPhoneNumber('');
            setAddress('');
        }
    }
    const handleFirstNameChange =(event) => {
        const value = event.target.value;
        setFirstName(value);
    }
    const handleLastNameChange =(event) => {
        const value = event.target.value;
        setLastName(value);
    }
    const handlePhoneNumberChange =(event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }
    const handleAddressChange =(event) => {
        const value = event.target.value;
        setAddress(value);
    }
    return(
        <div className='row'>
        <div className="offset-e col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create Customer</h1>
                <form onSubmit={handleSubmit} id="create-manufacturer-form">
                    <div className="form-floating mb-3">
                        <input placeholder="First Name" onChange={handleFirstNameChange} value={firstName}  required type="text" name="first_name" id="first_name" className="form-control" />
                        <label htmlFor="first_name">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input placeholder="Last Name" onChange={handleLastNameChange} value={lastName}  required type="text" name="last_name" id="last_name" className="form-control" />
                        <label htmlFor="last_name">Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input placeholder="Phone Number" onChange={handlePhoneNumberChange} value={phoneNumber}  required type="text" name="phone_number" id="phone_number" className="form-control" />
                        <label htmlFor="phone_number">Phone Number</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input placeholder="Address" onChange={handleAddressChange} value={address}  required type="text" name="address" id="address" className="form-control" />
                        <label htmlFor="address">Address</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    )
}

export default CustomerForm
