import React, {useEffect, useState} from 'react';

function CustomerList() {
    const [customerList, setCustomers] = useState([]);

    const fetchCustomers = async () => {
        const customersUrl = "http://localhost:8090/api/customers/"
        console.log("Whayyyyy")
        const response = await fetch(customersUrl);
        if (response.ok) {
            const data = await response.json()
            setCustomers(data.customers)
        }
    };

    useEffect(() => {
        fetchCustomers();
    }, []);

    return (
        <main className="container">
            <h1 className="h1-padded">Customers</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customerList.map(customer => {
                        return (
                            <tr key={customer.id}>
                                <td>{customer.first_name}</td>
                                <td>{customer.last_name}</td>
                                <td>{customer.phone_number}</td>
                                <td>{customer.address}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </main>
    )
}

export default CustomerList
