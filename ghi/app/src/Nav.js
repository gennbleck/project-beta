import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Auto Tech</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
							<NavLink className="nav-link" to="/automobiles">
								Automobiles
							</NavLink>
              <NavLink className="nav-link" to="/automobiles/new">Create Automobile</NavLink>
              </li>
              <li className="nav-item">
							<NavLink className="nav-link" to="/manufacturers/">
								Manufacturers
							</NavLink>
              <NavLink className="nav-link" to="/manufacturers/new/">
								Create a Manufacturer
							</NavLink>
              </li>
              <li className="nav-item">
							<NavLink className="nav-link" to="/models/">
								Models
							</NavLink>
              <NavLink className="nav-link" to="/models/new/">
								Create Model
							</NavLink>
						</li>
            <li className="nav-item">
							<NavLink className="nav-link" to="/technicians/">
								Technicians
							</NavLink>
              <NavLink className="nav-link" to="/technicians/new/">
								Create Technicians
							</NavLink>
              </li>
              <li className="nav-item">
							<NavLink className="nav-link" to="/appointments/">
								Appointments
							</NavLink>
              <NavLink className="nav-link" to="/appointments/new/">
								Appointments form
							</NavLink>
              </li>
              <li className="nav-item">
							<NavLink className="nav-link" to="/salespeople/">
								Salespeople
							</NavLink>
							<NavLink className="nav-link" to="/salespeople/new/">
								Create Salesperson
							</NavLink>
              </li>
              <li className="nav-item">
							<NavLink className="nav-link" to="/customers/">
								Customers
							</NavLink>
							<NavLink className="nav-link" to="/customers/new/">
								Create Customer
							</NavLink>
              </li>
              <li className="nav-item">
							<NavLink className="nav-link" to="/sales/">
								Sales
							</NavLink>
							<NavLink className="nav-link" to="/sales/new/">
								Create Sale
							</NavLink>
              </li>
			  <li className="nav-item">
							<NavLink className="nav-link" to="/sales/history/">
								Sale History
							</NavLink>
							<NavLink className="nav-link" to="/appointments/history/">
								Appointments History
							</NavLink>
              </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
