import { useState, useEffect } from 'react';

export default function ServiceHistoryList() {
  const [appointments, setAppointments] = useState([]);
  const [filterVin, setFilterVin] = useState('');

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/');
    const responseVIP = await fetch('http://localhost:8100/api/automobiles/');

    if (response.ok && responseVIP.ok) {
      const data = await response.json();
      const vipData = await responseVIP.json();

      const appointmentData = data.appointments.map(appointment => {
        return {
          id: appointment.id,
          vin: appointment.vin,
          customer: appointment.customer,
          date: new Date(appointment.date_time).toLocaleString().substring(0, 9),
          time: new Date(appointment.date_time).toLocaleString().substring(11),
          technician: appointment.technician.first_name,
          reason: appointment.reason,
          isCompleted: appointment.is_completed,
          isVip: appointment.isVip,
          status: appointment.status,
        };
      });

      setAppointments(appointmentData);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSearch = async () => {
    if (filterVin === '') {

      getData();
    } else {

      const filteredAppointments = appointments.filter(appointment =>
        appointment.vin.toLowerCase().includes(filterVin.toLowerCase())
      );

      setAppointments(filteredAppointments);
    }
  };

  const handleChange = event => {
    setFilterVin(event.target.value);
  };

  return (
    <main className="container">
      <h1 className="h1-padded">Service History</h1>
      <div className="form-outline">
        <input
          placeholder="Type in VIN"
          value={filterVin}
          onChange={handleChange}
        />
        <button onClick={handleSearch}>Search</button>
      </div>

      <table className="table">
        <thead>
          <tr>
            <th scope="col">Vin</th>
            <th scope="col">Is VIP?</th>
            <th scope="col">Customer</th>
            <th scope="col">Date</th>
            <th scope="col">Time</th>
            <th scope="col">Technician</th>
            <th scope="col">Reason</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.isVip ? 'Yes' : 'No'}</td>
                <td>{appointment.customer}</td>
                <td>{appointment.date}</td>
                <td>{appointment.time}</td>
                <td>{appointment.technician}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
                <td></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </main>
  );
}