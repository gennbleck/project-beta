import React, { useState, useEffect } from 'react';

function SaleForm () {
    const [ autos, setAutomobiles ] = useState ([]);
    const [ automobile, setAutomobile ] = useState ('');
    const [ salespeople, setSalespeople ] = useState ([]);
    const [ salesperson, setSalesperson ] = useState ('');
    const [ customers, setCustomers ] = useState ([]);
    const [ customer, setCustomer ] = useState ('');
    const [ price, setPrice ] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.automobile = automobile;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;

        const saleUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(saleUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            const newSale = await response.json();

            setAutomobile('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
        }
    }
    const fetchAutomobiles = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }
    const fetchSalespeople = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }
    const fetchCustomers = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }
    const handleAutoChange =(event) => {
        const value = event.target.value;
        setAutomobile(value);
    }
    const handleSalespersonChange =(event) => {
        const value = event.target.value;
        setSalesperson(value);
    }
    const handleCustomerChange =(event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const handlePriceChange =(event) => {
        const value = event.target.value;
        setPrice(value);
    }
    useEffect(() => {
        fetchAutomobiles();
        fetchSalespeople();
        fetchCustomers();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create New Sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
            <div className="mb-3">
              <select onChange={handleAutoChange} value={automobile} required name="automobile" id="automobile" className="form-select">
                <option value="">Choose VIN</option>
                {autos
                    .filter(automobile => !automobile.sold)
                    .map(automobile => {
                    return (
                        <option value={automobile.id} key={automobile.id}>
                            {automobile.vin}
                        </option>

                    );
                  })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                <option value="">Choose Salesperson</option>
                {salespeople.map(salesperson => {
                    return (
                        <option value={salesperson.id} key={salesperson.id}>
                            {salesperson.first_name + " " + salesperson.last_name}
                        </option>

                    );
                  })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
                <option value="">Choose Customer</option>
                {customers.map(customer => {
                    return (
                        <option value={customer.id} key={customer.id}>
                            {customer.first_name + " "+ customer.last_name }
                        </option>

                    );
                  })}
              </select>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handlePriceChange} value={price} placeholder="Price" required type="text" name="price" id="price" className="form-control"/>
                <label htmlFor="price">Price</label>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
        </div>
    )



}

export default SaleForm
